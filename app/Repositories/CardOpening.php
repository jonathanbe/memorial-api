<?php

namespace App\Repositories;

use App\Models\Card;

class CardOpening implements CardRepository {

    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentTask constructor.
     *
     * @param App\Task $model
     */
    public function __construct(Card $model) {
        $this->model = $model;
    }

    /**
     * Get all tasks.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getAll() {
        return $this->model->where('tipo', 'abertura')->get();
    }

    /**
     * Get task by id.
     *
     * @param integer $id
     *
     * @return App\Task
     */
    public function getById($id) {
        return $this->model->where(['id' => $id, 'tipo' => 'abertura'])->get();
    }

    /**
     * Create a new task.
     *
     * @param array $attributes
     *
     * @return App\Task
     */
    public function create(array $attributes) {
        $card = $this->model->create();

        $card->tipo = 'abertura';
        $card->data = $attributes['data'];
        $card->descricao_data = $attributes['descricao_data'];
        $card->titulo = $attributes['titulo'];
        $card->subtitulo = $attributes['subtitulo'];
        $card->texto = $attributes['texto'];
        $card->intervalo_inicio = $attributes['inicio'];
        $card->intervalo_fim = $attributes['fim'];
        $card->descricao_busca = $attributes['descricao_busca'];

        return $card;
    }

    /**
     * Update a task.
     *
     * @param integer $id
     * @param array $attributes
     *
     * @return App\Task
     */
    public function update($id, array $attributes) {
        $card = $this->model->find($id);

        $card->texto = $attributes['texto'];
        $card->titulo = $attributes['titulo'];
        $card->subtitulo = $attributes['subtitulo'];

        $card->save();

        return $card;
    }

    /**
     * Delete a task.
     *
     * @param integer $id
     *
     * @return boolean
     */
    public function delete($id) {
        return $this->model->find($id)->delete();
    }

}
