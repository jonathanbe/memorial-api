<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CardComplex;

class CardComplexController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('oauth:manage_card');
    }

    /**
     * Paginate resource.
     *
     * @return void
     * @return [\App\Models\CardComplex]
     */
    public function index() {
        $cards = CardComplex::paginate();

        return response($cards, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \App\Models\CardComplex
     */
    public function show($id) {
        $card = CardComplex::find($id);

        return response($card, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\CardComplex
     */
    public function store(Request $request) {
        $input = $request->input('card');

        try {
            $card = CardComplex::create($input);
            
            dd($card);
        } catch (\Exception $e) {
            return response($e->getMessage(), 401);
        }

        return response($card, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\CardComplex
     */
    public function update(Request $request, $id) {
        $input = $request->input('card');

        try {
            $card = CardComplex::find($id);
            $card->update($input);
        } catch (\Exception $e) {
            return response($e->getMessage(), 401);
        }

        return response($card, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id) {
        return response(CardComplex::destroy($id), 200);
    }
}
