<?php 

namespace App\Http\Controllers;
 
//use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

 
//use Request;
 
class FileController extends Controller
{
    
    private $disk;
    
    public function __construct()
    {
        $this->middleware('oauth:manage_file');
        $this->disk = 'public';
//        $this->strg = new Storage();
//        $this->strg->disk('public');
        
    }
    
    public function store(Request $request)
    {   
        $file = $request->file('file');
        Storage::disk($this->disk)->put($file->getClientOriginalName(), File::get($file));
 
        return response()->json('success');
    }
 
    public function destroy($name)
    {
        Storage::disk($this->disk)->delete($name);
        return response()->json('success');
    }
 
    public function getFileList(){
 
        $files = Storage::disk($this->disk)->files('/');
        return response()->json($files);
 
    }
 
    public function show($name){
 
        return response()->make(Storage::disk($this->disk)->get($name), 200, [
            'Content-Type' => Storage::mimeType($name),
            'Content-Disposition' => 'inline; '.$name,
        ]);
 
    }
 
}