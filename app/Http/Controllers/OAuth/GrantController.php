<?php

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use App\Models\User;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class GrantController extends Controller
{
    /**
     * Grant access through the password grant type.
     *
     * @param string $username
     * @param string $password
     * @return integer|boolean
     */
    public function password($username, $password)
    {
        $user = User::where([
            ['email', $username],
            ['senha', sha1($password)]
        ])->first();

        if ($user) {
            return $user->id;
        }

        return false;
    }
}
