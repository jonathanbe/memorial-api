<?php

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use App\Models\User;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Illuminate\Support\Facades\Auth;

class AccessTokenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('oauth', ['only' => 'auth']);
    }

    /**
     * Generate OAuth access token.
     *
     * @return string
     */
    public function token()
    {
        return Authorizer::issueAccessToken();
    }

    /**
     * Get authenticated user.
     *
     * @return \App\Models\User
     */
    public function auth()
    {
        return Auth::user();
    }
}
