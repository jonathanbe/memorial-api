<?php

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use App\Models\OAuth\Scope;
use Illuminate\Http\Request;

class ScopeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('oauth:manage_oauth');
    }

    /**
     * List resource.
     *
     * @return void
     */
    public function index()
    {
        return response(Scope::all(), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\Scope]
     */
    public function show($id)
    {
        return response(Scope::find($id), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\OAuth\Scope
     */
    public function store(Request $request)
    {
        $input = $request->input('scope');
        $scope = new Scope();

        $scope->id = $input['id'];
        $scope->description = $input['description'];
        $scope->save();

        return response($scope, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\Scope
     */
    public function update(Request $request, $id)
    {
        $input = $request->input('scope');
        $scope = Scope::find($id);

        $scope->description = $input['description'];
        $scope->save();

        return response($scope, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Scope::destroy($id), 200);
    }
}
