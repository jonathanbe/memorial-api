<?php

namespace App\Console\Commands;

use App\Models\OAuth\Client;
use Illuminate\Console\Command;

class MakeClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:client {id} {scopes}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an OAuth client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scopes = explode(',', $this->argument('scopes'));
        $client = new Client();

        $client->id = $this->argument('id');
        $client->name = 'Artisan Client';
        $client->secret = 'omg123';
        $client->save();

        $client->scopes()->sync($scopes);

        $this->info('Client Secret: ' . $client->secret);
    }
}
