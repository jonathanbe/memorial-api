<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'v1'], function () use ($app) {
    // User
    $app->group(['prefix' => 'users'], function () use ($app) {
        $app->get('/', ['uses' => 'UserController@index']); // List
        $app->post('/', ['uses' => 'UserController@store']); // Add
        $app->get('/{id}', ['uses' => 'UserController@show']); // Show
        $app->put('/{id}', ['uses' => 'UserController@update']); // Update
        $app->delete('/{id}', ['uses' => 'UserController@destroy']); // Delete
    });

    // Timeline
    $app->group(['prefix' => 'timelines'], function () use ($app) {
        $app->get('/', ['uses' => 'TimelineController@index']); // List
        $app->post('/', ['uses' => 'TimelineController@store']); // Add
        $app->get('/{id}', ['uses' => 'TimelineController@show']); // Show
        $app->put('/{id}', ['uses' => 'TimelineController@update']); // Update
        $app->delete('/{id}', ['uses' => 'TimelineController@destroy']); // Delete
    });
    
     // Tag
    $app->group(['prefix' => 'tags'], function () use ($app) {
        $app->get('/', ['uses' => 'TagController@index']); // List
        $app->post('/', ['uses' => 'TagController@store']); // Add
        $app->get('/{id}', ['uses' => 'TagController@show']); // Show
        $app->put('/{id}', ['uses' => 'TagController@update']); // Update
        $app->delete('/{id}', ['uses' => 'TagController@destroy']); // Delete
    });
    
    // Card
    $app->group(['prefix' => 'cards'], function () use ($app) {
        $app->get('/search', ['uses' => 'CardController@search']); // Search
        // Card Opening
        $app->group(['prefix' => 'opening'], function () use ($app) {
            $app->get('/', ['uses' => 'CardOpeningController@index']); // List
            $app->post('/', ['uses' => 'CardOpeningController@store']); // Add
            $app->get('/{id}', ['uses' => 'CardOpeningController@show']); // Show
            $app->put('/{id}', ['uses' => 'CardOpeningController@update']); // Update
            $app->delete('/{id}', ['uses' => 'CardOpeningController@destroy']); // Delete
//            $app->get('/search', ['uses' => 'CardOpeningController@search']); // Search
            
//          Route::get('/search', function (Request $request) {
//              return App\Order::search($request->search)->get();
//          });
        });
        
        // Card Simple
        $app->group(['prefix' => 'simple'], function () use ($app) {
            $app->get('/', ['uses' => 'CardSimpleController@index']); // List
            $app->post('/', ['uses' => 'CardSimpleController@store']); // Add
            $app->get('/{id}', ['uses' => 'CardSimpleController@show']); // Show
            $app->put('/{id}', ['uses' => 'CardSimpleController@update']); // Update
            $app->delete('/{id}', ['uses' => 'CardSimpleController@destroy']); // Delete
        });
        
        // Card Outer
        $app->group(['prefix' => 'outer'], function () use ($app) {
            $app->get('/', ['uses' => 'CardOuterController@index']); // List
            $app->post('/', ['uses' => 'CardOuterController@store']); // Add
            $app->get('/{id}', ['uses' => 'CardOuterController@show']); // Show
            $app->put('/{id}', ['uses' => 'CardOuterController@update']); // Update
            $app->delete('/{id}', ['uses' => 'CardOuterController@destroy']); // Delete
        });
        
        
        // Card Complex
        $app->group(['prefix' => 'complex'], function () use ($app) {
            $app->get('/', ['uses' => 'CardComplexController@index']); // List
            $app->post('/', ['uses' => 'CardComplexController@store']); // Add
            $app->get('/{id}', ['uses' => 'CardComplexController@show']); // Show
            $app->put('/{id}', ['uses' => 'CardComplexController@update']); // Update
            $app->delete('/{id}', ['uses' => 'CardComplexController@destroy']); // Delete
        });

        $app->get('/', ['uses' => 'CardController@index']); // List
        $app->get('/latest', ['uses' => 'CardController@latest']); // Get Latest Updated
        $app->get('/{id}', ['uses' => 'CardController@show']); // Show
        $app->delete('/{id}', ['uses' => 'CardController@destroy']); // Delete
    });
    
    //File
    $app->group(['prefix' => 'files'], function () use ($app) {
        $app->post('/', ['uses' => 'FileController@store']);
        $app->get('/list', ['uses' => 'FileController@getFileList']);
        $app->get('/{filename}', ['uses' => 'FileController@show']);
        $app->delete('/{filename}', ['uses' => 'FileController@destroy']);
    });

    // OAuth
    $app->group(['prefix' => 'oauth'], function () use ($app) {
        $app->post('access_token', ['uses' => 'OAuth\AccessTokenController@token']);
        $app->get('auth', ['uses' => 'OAuth\AccessTokenController@auth']);

        // Client
        $app->get('clients', ['uses' => 'OAuth\ClientController@index']); // List
        $app->post('clients', ['uses' => 'OAuth\ClientController@store']); // Add
        $app->get('clients/{id}', ['uses' => 'OAuth\ClientController@show']); // Show
        $app->put('clients/{id}', ['uses' => 'OAuth\ClientController@update']); // Update
        $app->delete('clients/{id}', ['uses' => 'OAuth\ClientController@destroy']); // Delete

        // Scope
        $app->get('scopes', ['uses' => 'OAuth\ScopeController@index']); // List
        $app->post('scopes', ['uses' => 'OAuth\ScopeController@store']); // Add
        $app->get('scopes/{id}', ['uses' => 'OAuth\ScopeController@show']); // Show
        $app->put('scopes/{id}', ['uses' => 'OAuth\ScopeController@update']); // Update
        $app->delete('scopes/{id}', ['uses' => 'OAuth\ScopeController@destroy']); // Delete
    });
});

