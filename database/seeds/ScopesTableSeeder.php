<?php

use Illuminate\Database\Seeder;

class ScopesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_scopes')->insert([
            'id' => 'manage_oauth',
            'description' => 'Read and write OAuth clients and scopes.'
        ]);

        DB::table('oauth_scopes')->insert([
            'id' => 'manage_user',
            'description' => 'Read and write users.'
        ]);

        DB::table('oauth_scopes')->insert([
            'id' => 'manage_card',
            'description' => 'Read and write cards.'
        ]);

        DB::table('oauth_scopes')->insert([
            'id' => 'manage_timeline',
            'description' => 'Read and write timeline.'
        ]);
    }
}
